from setuptools import setup
import sdist_upip

setup(
    cmdclass={'sdist': sdist_upip.sdist},

    name='micropython-mtx',
    py_modules=['mtx'],

    version='1',

    description='Extra-fast Matrix Multiplication and Linear System Solver on MicroPython',

    long_description='Extra-fast Matrix Multiplication and Linear System Solver on MicroPython',

    url='https://gitlab.com/nickoala/micropython-mtx',

    author='Nick Lee',
    author_email='lee1nick@yahoo.ca',

    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 5 - Production/Stable',

        'Intended Audience :: Developers',
        'Topic :: Software Development :: Embedded Systems',
        'Topic :: Scientific/Engineering :: Mathematics',

        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
    ],

    keywords='micropython openmv matrix',
)
